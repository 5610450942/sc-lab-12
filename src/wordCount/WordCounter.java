package wordCount;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;


public class WordCounter {
	private String filename;
	private String message;
	private HashMap<String,Integer> map;
	public WordCounter(String filename){
		this.filename = filename;
		map = new HashMap<String,Integer>();
		LoadingMassage();
	}
	public void LoadingMassage(){
		BufferedReader bufferReader = null;
		try{
			FileReader fileReader = new FileReader(filename);
			 bufferReader = new BufferedReader(fileReader);
			filename = "";
			String line = bufferReader.readLine();
			while(line != null){
				message += line;
				line = bufferReader.readLine();

			}
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally{try {
			bufferReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}}
	}
			 
	public void count(){
		for(String s : message.split(" ")){
			if(!map.containsKey(s)){
			map.put(s,1);
			}
			else{
				int a = map.get(s);
				map.replace(s, a, ++a);
			}
		}
	}
	public int hasWord(String word){
		if(map.containsKey(word)){
			return map.get(word);}
		else{
				return 0;
			}
		}
	public static void main(String[] args){
		WordCounter w = new WordCounter("poem.txt");
		w.count();
		System.out.print(w.getHasWord());
	}
	private String getHasWord() {
		String str = map.toString();
		str = str.replace(",","\n").replace("{","").replace("}","");
		return str;
	}


}
