package Phone;

import java.util.ArrayList;

public class Phone{
	private ArrayList<String> numbers;
	private String name;
	public Phone(String name,String number){
		numbers = new ArrayList<String>();
		this.name = name;
		this.numbers.add( number);
	}
	public String getName(){
		return name;
	}
	public void addPoneNumber(String number){
		numbers.add(number);
	}
	public String toString(){
		return name+", "+numbers;
	}
	public ArrayList<String> getPhone() {
		// TODO Auto-generated method stub
		return numbers;
	}
}
