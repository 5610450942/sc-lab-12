package Phone;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class PhoneBook {
	private ArrayList<Phone> phone ;
	public PhoneBook(){
		phone = new ArrayList<Phone>();
	}
	public String Loading() throws IOException{
		String filename = "phonebook.txt";
		 FileReader fileReader = new FileReader(filename);
		 BufferedReader bufferReader = new BufferedReader(fileReader);
		String  x = "Open file: "+filename;
				try {
					 String line = bufferReader.readLine();
					 /*FileWriter fileWriter = new FileWriter("phonebook.txt");
					 BufferedWriter bufferWriter = new BufferedWriter(fileWriter);*/
					 while(line != null){
						 String[] a = line.split(",");
						 String name = a[0].trim();
						 String number = a[1].trim();

						 addPhone(name,number);
						 x += "\n-----> " + line;
						 line = bufferReader.readLine();
					 }
			 	 } 
			 	 catch (IOException e){
					 System.err.println("Error reading from file");
			 	 }
				finally{bufferReader.close();}
				return x;
	}
	public String save() throws IOException{
		String filename = "phonebook.txt";
		 FileWriter fileWriter = new FileWriter(filename);
		 BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
		String x = "Write file: "+filename;
				try {
					for(Phone p : phone ){
						for(String n : p.getPhone()){
							bufferWriter.write(p.getName()+", "+n);
							bufferWriter.newLine();
						}
					}

			 	 } 
			 	 catch (IOException e){
					 System.err.println("Error reading from file");
			 	 }
				finally{
					bufferWriter.close();}
				return x += toString() ;
	}
	public void addPhone(String name,String number){
		Phone p2 =new Phone(name,number);
		boolean check = false;
		for(Phone p:phone){
			if(p.getName().equals(p2.getName())){
				p.addPoneNumber(number);
				check = true;
			};
		}
		if(check == false){
		phone.add(p2);
		}
	}
	public boolean delete(Phone p){
		return phone.remove(p);
	}
	public String toString(){
		return phone.toString();
	}
}
