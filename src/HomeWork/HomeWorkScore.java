package HomeWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class HomeWorkScore {
		private ArrayList<Student> students ;
		public HomeWorkScore(){
			students = new ArrayList<Student>();
		}
		public String Loading() throws IOException{
			String filename = "homework.txt";
			 FileReader fileReader = new FileReader(filename);
			 BufferedReader bufferReader = new BufferedReader(fileReader);
			String  x = "Open file: "+filename;
					try {
						 String line = bufferReader.readLine();
						 while(line != null){
							 String[] a = line.split(",");
							 addNewStu(a[0].trim(),a);
							 line = bufferReader.readLine();
						 } 
						 BufferedReader bfexam = new BufferedReader(new FileReader("exam.txt"));
						 line = bfexam.readLine();
						 while(line != null){
							 String[] a = line.split(",");
							 for(Student st: students){
								 if(st.getName().equals(a[0].trim())){
									 st.setExamScore(Double.parseDouble(a[1].trim()),Double.parseDouble( a[2].trim()));
								 }
							 }
							 line = bfexam.readLine();

						 } 
					}
				 	 catch (IOException e){
						 System.err.println("Error reading from file");
				 	 }
					finally{bufferReader.close();}
					return x+"\n"+toString();
		}
		public String saveAverage() throws IOException{
			String filename = "average.txt";
			 FileWriter fileWriter = new FileWriter(filename);
			 BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
			String x = "Write file: "+filename+"\n";
					try {
						bufferWriter.write("			Homework Scores ");
						bufferWriter.newLine();
						bufferWriter.write("----->name average ");
						bufferWriter.newLine();
						for(Student s : students ){
							x += s.getName()+" "+s.average()+"\n";
								bufferWriter.write("----->"+s.getName()+" "+s.average());
								bufferWriter.newLine();
							}
						bufferWriter.newLine();
						bufferWriter.newLine();
						bufferWriter.write( "----- Exam Score -----");
						bufferWriter.newLine();

						bufferWriter.write( "name average");
						bufferWriter.newLine();
						x +="\n\n----- Exam Score -----\nname average\n";
						for(Student s:students){
							x+=s.getName()+" "+s.getAvgExam()+"\n";
							bufferWriter.write(s.getName()+" "+s.getAvgExam());
							bufferWriter.newLine();
						}
						}

				 	 catch (IOException e){
						 System.err.println("Error reading from file");
				 	 }
					finally{
						bufferWriter.close();}
					return x  ;
		}
		public void addNewStu(String name,String[] point){
			 Student stu = new Student(name);
			 
			 for(int i = 1; i<point.length;i++){
				 stu.addPoint(Double.parseDouble(point[i].trim()));
		 }
			 students.add(stu);
		}
		public String toString(){
			String str = "";
			for(Student s:students){str+=s+"\n";}
			return str;
		}
}
