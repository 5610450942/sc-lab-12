package HomeWork;

import java.util.ArrayList;

public class Student {
	private String name;
	private double midTermScore;
	private double finalScore;

	private ArrayList<Double> points;
	public Student(String name) {
		this.name = name;
		points = new ArrayList<Double>();
	}

	public void addPoint(double p) {
		// TODO Auto-generated method stub
		points.add(p);
	}
	public String toString(){
		String str = name;
		for(double d:points){
			str += ", "+d;
		}
		return str+">> exam :"+midTermScore+" "+finalScore;
	}
	public double average(){
		double average = 0.0;
		for(double d:points){
			average +=d;
		}		
		return average/points.size();
	}
	public void setExamScore(double m,double f){
		midTermScore = m;
		finalScore = f;
	}
	public double getAvgExam(){
		return (midTermScore+finalScore)/2;
	}
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
}
